# VBA-Dictionary-Extension

VBA-Dictionary is a convenient and powerful drop-in replacement for Scripting.Dictionary for both Mac and Windows, based on a project created by Tim Hall and modified by Shimadonbey HIRAOKA Scripting.
It is designed to replace Scripting.Dictionary exactly, using Item as the default property (```Dict("A") = Dict.Item("A")```), matching error codes, and matching methods and properties.
We have also added an experimental but modified version of the For Each feature of the Dictionary object that was not implemented in the original project.

We would like to thank Mr. Tim Hall for his earlier publication of this excellent project.

Tim Hall's project:
https://github.com/VBA-tools/VBA-Dictionary


## Getting started

Download this repository. After downloading, please import ```vba-files/dictionary/Dictionary.cls``` into your VBA project.

## Examples

```VBA
' (Works exactly like Scripting.Dictionary)
Dim Dict As New Dictionary
Dict.CompareMode = CompareMethod.TextCompare

Dict("A") ' -> Empty
Dict("A") = 123
Dict("A") ' -> = Dict.Item("A") = 123
Dict.Exists "A" ' -> True

Dict.Add "A", 456
' -> Throws 457: This key is already associated with an element of this collection

' Both Set and Let work
Set Dict("B") = New Dictionary
Dict("B").Add "Inner", "Value"
Dict("B")("Inner") ' -> "Value"

UBound(Dict.Keys) ' -> 1
UBound(Dict.Items) ' -> 1

' Rename key
Dict.Key("B") = "C"
Dict.Exists "B" ' -> False
Dict("C")("Inner") ' -> "Value"

' Trying to remove non-existant key throws 32811
Dict.Remove "B"
' -> Throws 32811: Application-defined or object-defined error

' Trying to change CompareMode when there are items in the Dictionary throws 5
Dict.CompareMode = CompareMethod.BinaryCompare
' -> Throws 5: Invalid procedure call or argument

Dict.Remove "A"
Dict.RemoveAll

Dict.Exists "A" ' -> False
Dict("C") ' -> Empty
```

### License

This project applies MIT Licesence.
